﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {

    private Rigidbody rb;
    public float speed;

    //for scrolling background - SOURCE = https://unity3d.com/learn/tutorials/topics/2d-game-creation/2d-scrolling-backgrounds
    public float tileSizeZ;
    private Vector3 startPosition;

    // Use this for initialization
    void Start () {

        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward * speed;

        if (rb.tag == "Background")
        {
            startPosition = transform.position;
        }
        

    }
	
	// Update is called once per frame
	void Update () {

        if (rb.tag == "Bolt")
        {
            Vector3 gravity = Vector3.back * Time.deltaTime * 30; //Secondary Mechanic 2: Bolts have weight
            rb.velocity += gravity;
        }

        if (rb.tag == "Background")
        {
            float newPosition = Mathf.Repeat(Time.time * speed, tileSizeZ);
            transform.position = startPosition + Vector3.back * newPosition;
        }
    }
}


