﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour 
{
	//declaring variables for grabbing with getcomponent
	private Rigidbody rb;
    private AudioSource audioSource;

    //variables for ship movement
    public float speed;
    public Boundary boundary;
    public float tilt;
    public float gForce;

    //variables for shots
    public GameObject shot;
    public Transform shotSpawn; //can be called and unity will recognize as transform of game object
    public float fireRate;
    private float nextFire;

    //various
    public GameObject[] enemyArray;
    public float jumpForce;

    void Start() //called on initialization
	{
		//establish variable for grabbing rigidbody
		rb = GetComponent<Rigidbody>();

        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation); //creates an "instance" of a game object with a given transform
            audioSource.Play();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Vector3 up = transform.TransformDirection(Vector3.forward);
            rb.AddForce(up * jumpForce, ForceMode.Impulse); //source: https://answers.unity.com/questions/340033/help-with-a-simple-jump-script.html
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * 10 * speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * 10 * speed * Time.deltaTime);
        }

        //Allows pressing spacebar to count nearby enemies
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    enemyArray = GameObject.FindGameObjectsWithTag("Asteroid");
        //    print("Number of Asteroids: " + enemyArray.Length);
        //}

    }

    void FixedUpdate() //called just before each physics step
	{
        Vector3 gravity = Vector3.back * Time.deltaTime * gForce; //SECONDARY MECHANIC 1 - creates "gravity"

        rb.velocity += gravity; //movement * speed + gravity;

        rb.position = new Vector3 //Makes a vector to "clamp" the given boundaries
        (
            Mathf.Clamp (rb.position.x, boundary.xMin, boundary.xMax), 
            0.0f, 
            Mathf.Clamp (rb.position.z, boundary.zMin, boundary.zMax)
        ); 
        rb.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x * -tilt); // quaternion eulers are kinda like vectors, but for angles
	}
}
